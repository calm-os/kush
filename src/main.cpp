/*
 * Copyright 2020-2022 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <gio/gio.h>

#include <QApplication>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <QString>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    auto settings = g_settings_new("org.gnome.desktop.background");
    auto gwpurl = g_settings_get_string(settings, "picture-uri");
    QString wpurl{gwpurl};
    g_free(gwpurl);
    g_object_unref(settings);

    QQmlApplicationEngine engine;

    auto context = engine.rootContext();
    context->setContextProperty(QStringLiteral("PictureURL"), wpurl);

    engine.load(QUrl(QStringLiteral("qrc:///qml/main.qml")));

    return app.exec();
}

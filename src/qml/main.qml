/*
 * Copyright 2020-2022 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import QtWayland.Compositor
import QtWayland.Compositor.XdgShell
import "."

WaylandCompositor {
    id: compositor
    useHardwareIntegrationExtension: true

    WaylandOutput {
        sizeFollowsWindow: true
        window: Shell {
            id: shell
            compositor: compositor
        }
    }

    XdgShell {
        onToplevelCreated: function(toplevel, xdgSurface) {
            console.log("Got ID: %1".arg(toplevel.title));
            shell.surfaces.append({shellSurface: xdgSurface});
        }
    }
    XdgDecorationManagerV1 {
        preferredMode: XdgToplevel.ClientSideDecoration
    }

    QtTextInputMethodManager {}
}

/*
 * Copyright 2020-2022 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import QtQuick

Item {
    id: panelRoot
    visible: true
    state: "visible"

    function openTray() {
        panelRoot.focus = true;
        tray.focus = true;
        panelRoot.state = "tray";
    }

    function closeTray() {
        panelRoot.state = "visible";
    }

    Item {
        id: topPanel

        anchors.top: parent.top
        width: parent.width
        height: 32

        Rectangle {
            anchors.fill: parent
            color: "#000000"
            opacity: 0.78
        }

        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: false

            onClicked: {
                if (tray.visible) {
                    panelRoot.closeTray();
                } else {
                    panelRoot.openTray();
                }
            }
        }
    }

    Tray {
        id: tray

        anchors.right: parent.right
        width: 540
        height: parent.height - topPanel.height
        y: -parent.height
        visible: y > 0

        Keys.onEscapePressed: {
            panelRoot.closeTray();
        }
    }

    states: [
        State {
            name: "visible"
        },
        State {
            name: "tray"
            extend: "visible"
            PropertyChanges {
                target: tray
                y: topPanel.height
            }
        }
    ]

    transitions: [
        Transition {
            to: "visible"
        },
        Transition {
            to: "tray"
            NumberAnimation {
                target: tray
                duration: 150
                easing.type: Easing.OutCubic
                properties: "y"
            }
        }
    ]
}

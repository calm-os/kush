/*
 * Copyright 2020-2022 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import QtQuick
import QtQuick.Window
import QtQuick.Layouts
import QtWayland.Compositor

Window {
    id: shell

    title: "Kush"
    width: 540
    height: 960
    visible: true
    color: "#aaaaaa"

    LayoutMirroring.childrenInherit: true
    LayoutMirroring.enabled: Application.layoutDirection == Qt.RightToLeft

    property ListModel surfaces: ListModel {}
    property WaylandCompositor compositor: null

    Image {
        anchors.fill: parent
        clip: true
        smooth: true
        asynchronous: true
        fillMode: Image.PreserveAspectCrop
        source: PictureURL
    }

    WaylandMouseTracker {
        id: mouseTracker

        anchors.fill: parent
        windowSystemCursorEnabled: true

        Repeater {
            model: shell.surfaces
            ClientWindow {
                shellSurface: modelData

                onClientDestroyed: {
                    shell.surfaces.remove(index);
                }
            }
        }

        Item {
            id: overlay
            z: shell.surfaces.length + 2

            anchors.fill: parent

            Panel {
                id: panel
                anchors.fill: parent
            }
        }
    }
}

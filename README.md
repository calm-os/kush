# Kush

A Qt based graphical shell designed for convergence.

## Dependencies

Kush requires some other software to compile and run:

 - [cmake-extras](https://gitlab.com/calm-os/cmake-extras)
 - Qt >= 6.2 and subcomponents
   - QtQuick 2
   - QtQuick Controls 2
   - QtQuick Layouts
   - QtWaylandCompositor
   - QtSvg
 - gio
   - For the GSettings API to read wallpaper setting
 - gsettings-desktop-schemas
   - For the wallpaper setting, `picture-uri` key must be set to an image
